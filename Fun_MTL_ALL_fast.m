function  accu = Fun_MTL_ALL_fast(tr_patch, trlsd, tt_patch, ttls, dic_t, lambda_alpha, lambda)
patch_num = length(tr_patch); 
Test_NUM = length(ttls);
class_num = length(unique(trlsd{1}));

[tr_patch, trlsd, ~] = ExtendTraingUsingNearestNeighbors(tr_patch, trlsd, 4);
for k = 1:patch_num
    train_num = size(tr_patch{k},2);
    tr_patch{k} = [tr_patch{k} dic_t{k}];
    trlsd{k}    = [trlsd{k}; (max(trlsd{k})+1)*ones(size(dic_t{k},2),1)];
end
%%

%for computing alpha when fixing beta
Q = cell(patch_num, 1);
X = cell(patch_num, 1);
group_index = cell(patch_num, 1);
for k = 1:patch_num
    X{k} = tr_patch{k}'*tr_patch{k};%G'*G
    Q{k} = inv( X{k}+lambda_alpha*eye(size(tr_patch{k},2)) );
    group_index{k} = trlsd{k}';
end
opt.eta = 1e-4;
opt.lambda = lambda;
opt.ite_num = 3;
opt.kernel_view = 0;
opt.R  =  X;

%%
num_tongji  = 0;
time_tongji = 0;
for indTest = 1:1:Test_NUM
     tic;
%     fprintf([num2str(Test_NUM) ':' num2str(indTest) '\n']);
    
        %update alpha when beta is fixed
        Y = cell(patch_num, 1);
        for k = 1:patch_num
            Y{k} = tr_patch{k}'*tt_patch(:,k,indTest);%G'*testY
        end
        coeff = MTJSRC_APG_ALL_fast(X, Y, Q, group_index, opt);
        
    % classification
    error_patch = zeros(patch_num,class_num);
    for m = 1:patch_num
        
        y_temp = dic_t{m}*coeff{m}(train_num+1:end);
        for n = 1:class_num
            d_temp = tr_patch{m}(:,trlsd{m}==n);
            coee_tem = coeff{m}(trlsd{m}==n);
            coee_temp = [coee_tem;coeff{m}(train_num+1:end)];
            error_patch(m,n) = (norm(tt_patch(:,m,indTest)-d_temp*coee_tem-y_temp))^2/sum(coee_temp.*coee_temp);
            %error_patch(m,n)=(norm(tt_patch(:,m,indTest)-d_temp*coee_tem-y_temp))^2; % very terible
        end
    end
    error = sum(error_patch);
    [~,temp] = min(error);
    label(indTest) = temp;
    toc;
    time_tongji = time_tongji+toc;
    num_tongji = num_tongji + 1;
end
time_tongji/num_tongji
accu = sum(label==ttls)/length(ttls);


