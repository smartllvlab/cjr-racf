function tt_dat = NormalizeData(tt_dat)
tt_dat = double(tt_dat);
for image_k = 1 : size(tt_dat, 3)
    temp = tt_dat(:, :, image_k);
    temp = temp./ repmat(sqrt(sum(temp.*temp)),[size(temp,1) 1]);% unit norm 2
    tt_dat(:, :, image_k) = temp;
end
end

