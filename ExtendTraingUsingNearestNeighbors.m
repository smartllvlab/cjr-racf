function  [tr_patch_temp, trlsd_temp, P_neigh] = ExtendTraingUsingNearestNeighbors(tr_patch, trlsd, neigh_num)

patch_num = length(tr_patch);
dim = size(tr_patch{1}, 1);
atom_num = size(tr_patch{1}, 2);
tr_patch_temp = cell(1, patch_num);
trlsd_temp = cell(1, patch_num);
for k = 1 : patch_num
    tr_patch_temp{k} = zeros(dim, atom_num*(neigh_num+1));
    trlsd_temp{k} =  zeros(atom_num*(neigh_num+1), 1);
end
neighbors = cell(1,16); %each cell sore its nearest neighbors

switch neigh_num
    case 8
        %% extend the gallery by using 8 nearest neighbors
        neighbors{1} = [1 1 1 1 1 2 5 6];
        neighbors{2} = [2 2 2 1 3 5 6 7];
        neighbors{3} = [3 3 3 2 4 6 7 8];
        neighbors{4} = [4 4 4 4 4 3 7 8];
        neighbors{5} = [5 5 5 1 2 6 9 10];
        neighbors{6} = [1 2 3 5 7 9 10 11];
        neighbors{7} = [2 3 4 6 8 10 11 12];
        neighbors{8} = [8 8 8 8 3 4 7 11 12];
        neighbors{9} = [9 9 9 5 6 10 13 14];
        neighbors{10} = [5 6 7 9 11 13 14 15];
        neighbors{11} = [6 7 8 10 12 14 15 16];
        neighbors{12} = [12 12 12 7 8 11 15 16];
        neighbors{13} = [13 13 13 13 13 9 10 14];
        neighbors{14} = [14 14 14 9 10 11 13 15];
        neighbors{15} = [15 15 15 10 11 12 14 16];
        neighbors{16} = [16 16 16 16 16 11 12 15];
        neighbors{17} = [17 17 17 17 17 17 17 17];
        neighbors{18} = [18 18 18 18 18 18 18 18];
        neighbors{19} = [19 19 19 19 19 19 19 19];
        neighbors{20} = [20 20 20 20 20 20 20 20];
        neighbors{21} = [21 21 21 21 21 21 21 21];
%         neighbors{1} = [1 1 1 1 2 1 4 5];
%         neighbors{2} = [2 2 2 1 3 4 5 6];
%         neighbors{3} = [3 3 3 2 3 5 6 3];
%         neighbors{4} = [4 1 2 4 5 4 7 8];
%         neighbors{5} = [1 2 3 4 6 7 8 9];
%         neighbors{6} = [2 3 6 5 6 8 9 6];
%         neighbors{7} = [7 4 5 7 8 7 7 7];
%         neighbors{8} = [4 5 6 7 9 8 8 8 ];
%         neighbors{9} = [5 6 9 8 9 9 9 9];
%         neighbors{10} = [10 10 10 10 10 10 10 10];
    case 4
        % extend the gallery by using 4 nearest neighbors
        neighbors{1} = [1 1 2 5];
        neighbors{2} = [2 1 3 6];
        neighbors{3} = [3 2 4 7];
        neighbors{4} = [4 4 3 8];
        neighbors{5} = [5 1 6 9];
        neighbors{6} = [2 5 7 10];
        neighbors{7} = [3 6 8 11];
        neighbors{8} = [8 4 7 12];
        neighbors{9} = [9 5 10 13];
        neighbors{10} = [6 9 11 14];
        neighbors{11} = [7 10 12 15];
        neighbors{12} = [12 8 11 16];
        neighbors{13} = [13 13 9 14];
        neighbors{14} = [14 10 13 15];
        neighbors{15} = [15 11 14 16];
        neighbors{16} = [16 16 12 15];
        neighbors{17} = [17 17 17 17];
        neighbors{18} = [18 18 18 18];
        neighbors{19} = [19 19 19 19];
        neighbors{20} = [20 20 20 20];
        neighbors{21} = [21 21 21 21];
     

%         neighbors{1} = [1 1 2 3];
%         neighbors{2} = [2 1 2 4];
%         neighbors{3} = [1 3 4 3];
%         neighbors{4} = [2 3 4 4];
%         neighbors{5} = [5 5 5 5];
		
% 		neighbors{1} = [1 1 2 4];
%         neighbors{2} = [2 1 3 5];
%         neighbors{3} = [3 2 3 6];
%         neighbors{4} = [1 4 5 7];
%         neighbors{5} = [2 4 6 8];
%         neighbors{6} = [3 5 6 9];
%         neighbors{7} = [4 7 8 7];
%         neighbors{8} = [5 7 9 8];
%         neighbors{9} = [6 8 9 9];
%         neighbors{10} = [10 10 10 10];
end

for k = 1 : patch_num
    tr_patch_temp{k}(:, 1:(neigh_num+1):end) = tr_patch{k};
    trlsd_temp{k}(1:(neigh_num+1):end, 1) = trlsd{k};
    for ni = 2 : (neigh_num+1)
        tr_patch_temp{k}(:, ni:(neigh_num+1):end) = tr_patch{neighbors{k}(ni-1)};
        trlsd_temp{k}(ni:(neigh_num+1):end, 1) = trlsd{neighbors{k}(ni-1)};
    end
end
P_neigh = zeros(atom_num, atom_num*(neigh_num+1));
for h = 1 : atom_num
    P_neigh(h, (neigh_num+1)*(h-1)+1 : (neigh_num+1)*h) = 1;
end

end

