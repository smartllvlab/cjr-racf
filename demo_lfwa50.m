%%this file is based on demo_lfwa_acpr_extend_net
%i reimplement deepid method (and has the same experiment setting as PR special issue paper)
% 1-50 gallery
% 51-158 variation dictonary
%addpath(genpath('E:\demo_mtjsrc_OxfordFlower_17\source code'));
clear all;
clc;
addpath(genpath('KSVD_Matlab_ToolBox'));
load('lfw_label');
% clear lfw_data_matrix;

patchNum = 17;%16
% path = 'more_than_10_images\add_deepw_new\';
path = 'more_than_10_images\30G256\';

data_name = '30G256_2_';%GLOBAL2_,iccv_

lfw_data_patch = cell(1, patchNum);
for patch_k = 1 : patchNum
    feature = load([path data_name num2str(patch_k) '_deep.mat']);
    lfw_data_patch{patch_k} = feature.DeepID(1:256, :); %160
    %lfw_data_patch{patch_k} = feature.DeepID;
end

%% gallery set
is_gallery_multipatch_per_point = false;
if is_gallery_multipatch_per_point
    tr_patch = cell(1, patchNum);
    trlsd = cell(1, patchNum);
    for patch_k = 1 : 21
        tr_patch{patch_k} = [];
        trlsd{patch_k} = [];
        for ci = 1 : 50
            image_id = find(lfw_sample_labels==ci, 1, 'first');
            for n = 1 : 9
                patch_dat = lfw_data_multipatch{patch_k}(:, 9*(image_id-1)+n);
                patch_dat = double(patch_dat);%NOTE
                patch_dat = patch_dat/sqrt(patch_dat' * patch_dat);%normalization
                tr_patch{patch_k} = [tr_patch{patch_k} patch_dat];
                trlsd{patch_k} = [trlsd{patch_k}; ci];
            end
        end
    end
    %%patch 22 whole image
    if patchNum == 22;
        patch_k = 22;
        tr_patch{patch_k} = [];
        trlsd{patch_k} = [];
        for ci = 1 : 50
            for n = 1 : 9% each point has 9 patches, here for the whole image we repeat it  9times
                %patch_dat = Tr_dataMatrix_patch(:, patch_k, 1+7*(ci-1):1+7*(ci-1));
                patch_dat = lfw_data_patch{patch_k}(:, find(lfw_sample_labels==ci, 1, 'first'));
                patch_dat = double(patch_dat);%NOTE
                patch_dat = patch_dat/sqrt(patch_dat' * patch_dat);%normalization
                tr_patch{patch_k} = [tr_patch{patch_k} patch_dat];
                trlsd{patch_k} = [trlsd{patch_k}; ci];
            end
        end
    end
else
    tr_patch = cell(1, patchNum);
    trlsd = cell(1, patchNum);
    for patch_k = 1 : patchNum
        tr_patch{patch_k} = [];
        trlsd{patch_k} = [];
        for ci = 1 : 50
            %patch_dat = lfw_data_patch(:, patch_k, find(lfw_sample_labels==ci, 1, 'first'));
            patch_dat = lfw_data_patch{patch_k}(:, find(lfw_sample_labels==ci, 1, 'first'));
            patch_dat = double(patch_dat);%NOTE
            patch_dat = patch_dat/sqrt(patch_dat' * patch_dat);%normalization
            tr_patch{patch_k} = [tr_patch{patch_k} patch_dat];
            trlsd{patch_k} = [trlsd{patch_k}; ci];
        end
    end
end

%% testing set
Tr_dataMatrix_patch = [];
for patch_k = 1 : patchNum
    Tr_dataMatrix_patch(:, :, patch_k) = lfw_data_patch{patch_k};
end
Tr_dataMatrix_patch = permute(Tr_dataMatrix_patch,[1 3 2]);

tt_patch = []; ttls = [];
tt_patch_index = [];
for ci = 1:50
    temp_index = find(lfw_sample_labels == ci);
    temp_index = temp_index(2:end);
    tt_patch_index = [tt_patch_index temp_index];
    ttls  = [ttls repmat(ci,[1 length(temp_index)])];
end
tt_patch = Tr_dataMatrix_patch(:, :, tt_patch_index);
tt_patch = NormalizeData(tt_patch);
    
%% construct dictonary
dic_t = cell(1, patchNum);
%load('lfw_database\ACPR_dictionary_learn_100.mat');
for patch_k = 1 : patchNum
    for ci = 51:158
        %patch_dat = lfw_data_patch(:, patch_k, lfw_sample_labels==ci);
        patch_dat = lfw_data_patch{patch_k}(:, lfw_sample_labels==ci);
        patch_dat = double(patch_dat);%NOTE
        %patch_dat = reshape(patch_dat, [size(patch_dat, 1) size(patch_dat, 2)]);
        
        reference_dat = mean(patch_dat, 2);
        reference_dat = reference_dat/sqrt(reference_dat' * reference_dat);%normalization
        
        patch_dat = patch_dat./ repmat(sqrt(sum(patch_dat.*patch_dat)),[size(patch_dat,1) 1]); %normalization
        temp_atoms = patch_dat - repmat(reference_dat, [1 size(patch_dat, 2)]);
        dic_t{patch_k} = [dic_t{patch_k} temp_atoms];
    end
end
%%
canca_dic = [];
for patch_k = 1 : patchNum
    canca_dic = [canca_dic;  dic_t{patch_k}];
end

% KSVD
param.L = 3;   % number of elements in each linear combination.
param.K = 500; % number of dictionary elements
param.numIteration = 10; % number of iteration to execute the K-SVD algorithm.

param.errorFlag = 0; % decompose signals until a certain error is reached. do not use fix number of coefficients.
%param.errorGoal = sigma;
param.preserveDCAtom = 0;

% %%%%%% creating the data to train on %%%%%%%%
% % N = 1500; % number of signals to generate
% % n = 20;   % dimension of each data
% % SNRdB = 20; % level of noise to be added
% % [param.TrueDictionary, D, x] = gererateSyntheticDictionaryAndData(N, param.L, n, param.K, SNRdB);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%%%% initial dictionary: Dictionary elements %%%%%%%%
param.InitializationMethod =  'DataElements';
% 
param.displayProgress = 1;
disp('Starting to  train the dictionary');
[Dictionary,~]  = KSVD(canca_dic, param);

dic_dim = size(dic_t{1}, 1);
for patch_k = 1 : patchNum
    dic_t{patch_k} = Dictionary((patch_k-1)*dic_dim+1 : patch_k*dic_dim, :);
end

% load('more_than_10_images\LFW_DIC\lfw_dic1630G256.mat');
%%
disp('have started representation and classification');
lambda_alpha = 0.5;
tem_tr_patch = []; 
%res = Fun_MTL_ALL(tr_patch, trlsd, tt_patch, ttls, dic_t, lambda_alpha, 0.05);
res = Fun_MTL_ALL_fast(tr_patch, trlsd, tt_patch, ttls, dic_t, lambda_alpha, 0.05);


